package ru.myhelpit.vkplayer.data;

/**
 * Created by DL on 02.12.2017.
 */

public class UpdateInfoBean {
    private int versionCode;
    private String version;
    private long timeStamp;
    private String link;
    private int validVersionCode;

    public UpdateInfoBean(int versionCode, String version, long timeStamp, String link, int validVersionCode) {
        this.versionCode = versionCode;
        this.version = version;
        this.timeStamp = timeStamp;
        this.link = link;
        this.validVersionCode = validVersionCode;
    }

    public int getVersionCode() {
        return versionCode;
    }

    public String getVersion() {
        return version;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public String getLink() {
        return link;
    }

    public int getValidVersionCode() {
        return validVersionCode;
    }
}
