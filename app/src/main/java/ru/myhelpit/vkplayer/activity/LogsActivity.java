package ru.myhelpit.vkplayer.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import androidx.appcompat.app.AppCompatActivity;
import ru.myhelpit.vkplayer.R;
import ru.myhelpit.vkplayer.VkPlayerApplication;
import ru.myhelpit.vkplayer.utils.MyAppLogReader;
import ru.myhelpit.vkplayer.utils.PreferenceUtils;

import static ru.myhelpit.vkplayer.utils.MyAppLogReader.sendLog;

public class LogsActivity extends AppCompatActivity {

    private static final String TAG = "SettingsActivity";
    private TextView txtLog;
    private EditText textSearch;
    private Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logs);

        // Obtain the shared Tracker instance.
        VkPlayerApplication application = (VkPlayerApplication) getApplication();
        mTracker = application.getDefaultTracker();

        txtLog = findViewById(R.id.txtLog);
        textSearch = findViewById(R.id.editTextSearch);

        boolean isDebug = PreferenceUtils.isDebugMode(getApplicationContext());
        int visibility = isDebug ? View.VISIBLE : View.INVISIBLE;
        findViewById(R.id.btnShowLog).setVisibility(visibility);
        findViewById(R.id.linearLayoutSearch).setVisibility(visibility);
    }

    public void click(View view) {
        switch (view.getId()) {
            case R.id.btnShowLog:
                txtLog.setText(MyAppLogReader.getLog());
                txtLog.bringPointIntoView(0);
                break;
            case R.id.btnSendLog:
                sendLog(this);
                break;
            case R.id.imageButtonSearch:
                findText();
        }
    }

    private void findText() {
        String text = textSearch.getText().toString();
        if ("".equals(text)){
            return;
        }
        if ("".equals(txtLog.getText())){
            return;
        }

        String logs = txtLog.getText().toString();
        int index = logs.indexOf(text, txtLog.getSelectionStart());
        if (index != -1){
            txtLog.bringPointIntoView(index);
        }

    }

    @Override
    protected void onResume() {
        Log.i(TAG, "Setting screen name: " + TAG);
        mTracker.setScreenName("Image~" + TAG);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        super.onResume();
    }
}
