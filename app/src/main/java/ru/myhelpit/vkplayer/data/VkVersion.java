package ru.myhelpit.vkplayer.data;

import java.util.Date;

public class VkVersion {
    private Integer versionCode;
    private String versionName;
    private Date buildDate;
    private String link;
    private Integer validVersion;

    public Integer getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(Integer versionCode) {
        this.versionCode = versionCode;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public Date getBuildDate() {
        return buildDate;
    }

    public void setBuildDate(Date buildDate) {
        this.buildDate = buildDate;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Integer getValidVersion() {
        return validVersion;
    }

    public void setValidVersion(Integer validVersion) {
        this.validVersion = validVersion;
    }
}
