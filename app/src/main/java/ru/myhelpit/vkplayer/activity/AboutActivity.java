package ru.myhelpit.vkplayer.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import androidx.appcompat.app.AlertDialog;
import ru.myhelpit.vkplayer.BuildConfig;
import ru.myhelpit.vkplayer.Constant;
import ru.myhelpit.vkplayer.R;
import ru.myhelpit.vkplayer.VkPlayerApplication;
import ru.myhelpit.vkplayer.task.CheckUpdateTask;
import ru.myhelpit.vkplayer.utils.Serializer;
import ru.myhelpit.vkplayer.utils.PreferenceUtils;

import static ru.myhelpit.vkplayer.utils.DateUtils.getBuildDate;

public class AboutActivity extends Activity {

    private static final String TAG = "AboutActivity";
    TextView version;
    TextView txtInfo;
    private Tracker mTracker;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        // Obtain the shared Tracker instance.
        VkPlayerApplication application = (VkPlayerApplication) getApplication();
        mTracker = application.getDefaultTracker();

        initViews();
    }

    private void initViews() {
        version = findViewById(R.id.version);
        Log.i(TAG,"versionCode: "+BuildConfig.VERSION_CODE);
        version.setText(BuildConfig.VERSION_NAME);

        Log.i(TAG, "buildDate: "+ getBuildDate()+ "\ntimeStamp: "+getBuildDate().getTime());
        ((TextView)findViewById(R.id.txtProjectLink))
                .setText(getResources().getString(R.string.project_link)+": "+ Constant.PROJECT_LINK);
        txtInfo = findViewById(R.id.txtInfo);
    }

    public void onClick(View view){
        switch (view.getId()){
            case R.id.btnCheckUpdate:
                checkUpdate();
                break;
            case R.id.btnCleanMeta:
                cleanMeta();
                break;
            case R.id.btnForgetUser:
                PreferenceUtils.removeUserData(getApplication());
                Serializer.removeSerializable(getApplicationContext(), Constant.COOKIE_FILE);
                break;
        }
    }

    private void checkUpdate(){
        CheckUpdateTask update = new CheckUpdateTask(getApplicationContext(), txtInfo);
        update.execute();
    }

    private void cleanMeta(){
        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.clean_meta_title))
                .setMessage(getString(R.string.clean_meta_msg))
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        Serializer.removeSerializable(getApplicationContext(), Constant.PLAY_LIST_DATA_FILE);
                        finishAffinity();
                    }
                }).create().show();

    }

    @Override
    protected void onResume() {
        Log.i(TAG, "Setting screen name: " + TAG);
        mTracker.setScreenName("Image~" + TAG);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        super.onResume();
    }


}