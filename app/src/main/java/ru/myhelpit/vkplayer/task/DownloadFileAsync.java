package ru.myhelpit.vkplayer.task;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.PowerManager;
import android.util.Log;
import android.widget.ProgressBar;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import ru.myhelpit.common.M3uUtils;
import ru.myhelpit.vkplayer.R;
import ru.myhelpit.vkplayer.data.RecordExt;
import ru.myhelpit.vkplayer.utils.Notifier;

import static ru.myhelpit.vkplayer.Constant.VK_AUDIO_FOLDER;
import static ru.myhelpit.vkplayer.Constant.VK_TEMP_FOLDER;

/**
 * Created by DL on 27.11.2017.
 */

public class DownloadFileAsync extends AsyncTask<RecordExt, Integer, String> {
    private final VkCallback callback;
    private Context context;
    private PowerManager.WakeLock wakeLock;

    private static final File FOLDER = new File(Environment.getExternalStorageDirectory(), VK_AUDIO_FOLDER);
    private static final File TEMP_FOLDER = new File(FOLDER, VK_TEMP_FOLDER);
    private ProgressBar bar;


    private final static String TAG = DownloadFileAsync.class.getCanonicalName();

    public DownloadFileAsync(Context context, ProgressBar bar, VkCallback callback) {
        this.context = context;
        this.bar = bar;
        this.callback = callback;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Log.d(TAG, VK_AUDIO_FOLDER + "vkAudio folder exists: " + FOLDER.exists());
        if (!FOLDER.exists()) {
            FOLDER.mkdir();
        }

        if (!TEMP_FOLDER.exists()){
            TEMP_FOLDER.mkdir();
        }

        bar.setProgress(0);
        bar.setMax(100);

        // take CPU lock to prevent CPU from going off if the user
        // presses the power button during download
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                getClass().getName());
        wakeLock.acquire();
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        wakeLock.release();
        if (result != null) {
            Log.e(TAG, "download failed: " + result);
            Notifier.show(context, "Download error: " + result);
        } else {
            Log.d(TAG, "file downloaded successful!");
            Notifier.show(context, "file downloaded successful!");
        }
        if (callback != null) {
            callback.callback();
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        bar.setProgress(values[0]);
    }

    /**
     * you can pass an array of record but UI will be updated only after downloading all files
     */
    @Override
    protected String doInBackground(RecordExt... records) {
        InputStream inputStream = null;
        OutputStream outputStream = null;
        HttpURLConnection connection = null;
        if (records == null || records.length == 0) {
            return "No records to download";
        }

        for (RecordExt record : records) {
            try {

                if(record.isM3u8()) {
                    publishProgress(33);
                    File file = new File(FOLDER, record.getVkId());
                    file.createNewFile();
                    M3uUtils.downloadPlayList(TEMP_FOLDER, record.getUrl(), file, false);
                    if (file.length() > 0) {
                        record.setFilePath(file.getAbsolutePath());
                        record.setSelected(false);
                    } else {
                        Log.i(TAG, "file size is zero. file will be deleted");
                        file.delete();
                    }
                    publishProgress(100);
                    Log.d(TAG, "Download from m3u9 finished." );

                }else{
                    URL url = new URL(record.getUrl());
                    connection = (HttpURLConnection) url.openConnection();
                    connection.connect();
                    if (HttpURLConnection.HTTP_NOT_FOUND == connection.getResponseCode()) {
                        return context.getString(R.string.update_audio_links);
                    }
                    if (HttpURLConnection.HTTP_OK != connection.getResponseCode()) {
                        return "Server returned " + connection.getResponseCode()
                                + " " + connection.getResponseMessage();
                    }

                    int fileSize = connection.getContentLength();
                    if (fileSize > 0) {
                        //switch an infinite animation off
                        bar.setIndeterminate(false);
                    }
                    inputStream = connection.getInputStream();
                    File file = new File(FOLDER, record.getVkId());
                    file.createNewFile();

                    Log.d(TAG, String.format("is file [%s] created? %s", file.getAbsolutePath(), file.exists()));
                    outputStream = new FileOutputStream(file, false);

                    byte[] data = new byte[1024];
                    long total = 0;
                    int count;
                    while ((count = inputStream.read(data)) != -1) {
                        if (isCancelled()) {
                            inputStream.close();
                            return null;
                        }
                        total += count;
                        if (fileSize > 0) {
                            publishProgress((int) (total * 100 / fileSize));
                        }
                        outputStream.write(data, 0, count);
                    }
                    record.setFilePath(file.getAbsolutePath());
                    record.setSelected(false);
                    Log.d(TAG, String.format("Download finished. Total %s bytes", total));
                }
            } catch (Exception e) {
                Log.e(TAG, "can't load from: " + record.getUrl(), e);
                return "can't load from: " + record.getUrl() + " " + e.getMessage();
            } finally {
                try {
                    if (outputStream != null) {
                        outputStream.close();
                    }
                    if (inputStream != null) {
                        inputStream.close();
                    }
                } catch (IOException e) {
                    Log.e(TAG, "an error occurred while attempting to close stream.", e);
                }
                if (connection != null) {
                    connection.disconnect();
                }
            }
        }
        deleteDirectory(TEMP_FOLDER);
        return null;
    }

    private void deleteDirectory(File dir){
        if (dir == null || !dir.exists()){
            return;
        }
        if (dir.isDirectory()){
            for (File file: dir.listFiles()){
                if (file.isDirectory()){
                    deleteDirectory(file);
                } else {
                    file.delete();
                }
            }
        }
    }
}
