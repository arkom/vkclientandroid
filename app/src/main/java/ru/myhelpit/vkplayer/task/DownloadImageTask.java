package ru.myhelpit.vkplayer.task;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;

import org.apache.commons.collections4.CollectionUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collection;

import ru.myhelpit.vkplayer.data.RecordExt;

import static ru.myhelpit.vkplayer.Constant.IMG_EXT;
import static ru.myhelpit.vkplayer.Constant.SUFFIX_NORMAL_ALBUM_IMG;
import static ru.myhelpit.vkplayer.Constant.SUFFIX_SMALL_ALBUM_IMG;
import static ru.myhelpit.vkplayer.Constant.VK_AUDIO_FOLDER;
import static ru.myhelpit.vkplayer.Constant.VK_IMG_FOLDER;

/**
 * Created by DL on 18.12.2017.
 */

public class DownloadImageTask extends AsyncTask<Collection<RecordExt>, Integer, String> {
    private Context context;
    private File imgFolder;
    private final static String TAG = DownloadImageTask.class.getCanonicalName();

    private InputStream inputStream = null;
    private OutputStream outputStream = null;
    private HttpURLConnection connection = null;
    private final VkCallback callback;

    public DownloadImageTask(Context context, VkCallback callback) {
        this.context = context;
        this.callback = callback;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        File audioFolder = new File(Environment.getExternalStorageDirectory(), VK_AUDIO_FOLDER);
        imgFolder = new File(audioFolder, VK_IMG_FOLDER);
        boolean mkDirs = imgFolder.mkdirs();
        Log.d(TAG, "mkDirs: " + mkDirs);
    }

    @Override
    protected void onPostExecute(String s) {
        if (callback != null){
            callback.callback();
        }
        super.onPostExecute(s);
    }

    @Override
    protected String doInBackground(Collection<RecordExt>... params) {
        if (params == null || params.length == 0 || CollectionUtils.isEmpty(params[0])) {
            return "no records to download img";
        }
        if (!imgFolder.exists()) {
            return String.format("img folder %s is not exist", imgFolder.getAbsolutePath());
        }

        String error = "";
        String sUrl = null;
        for (RecordExt record : params[0]) {
            try {
                sUrl = record.getImg40x40Url();
                if (record.getImage40x40Path() == null && !TextUtils.isEmpty(sUrl)) {
                    File file = saveToFile(record.getVkId() + SUFFIX_SMALL_ALBUM_IMG + IMG_EXT, sUrl);
                    if (file != null) {
                        record.setImage40x40Path(file.getAbsolutePath());
                    }
                }

                sUrl = record.getImg80x80Url();
                if (record.getImage80x80Path() == null && !TextUtils.isEmpty(sUrl)) {
                    release();
                    File file = saveToFile(record.getVkId() + SUFFIX_NORMAL_ALBUM_IMG + IMG_EXT, sUrl);
                    if (file != null) {
                        record.setImage80x80Path(file.getAbsolutePath());
                    }
                }
            } catch (Exception e) {
                Log.e(TAG, String.format("unable to download file at %s . Record: ",sUrl,record) , e);
                error+= e.getMessage()+"\n";
            } finally {
                release();
            }
        }
        return error;
    }

    private void release() {
        try {
            if (outputStream != null) {
                outputStream.close();
                outputStream = null;
            }
            if (inputStream != null) {
                inputStream.close();
                inputStream = null;
            }
        } catch (IOException e) {
            Log.e(TAG, "an error occurred while attempting to close stream.", e);
        }
        if (connection != null) {
            connection.disconnect();
            connection = null;
        }
    }

    private File saveToFile(String fileName, String sUrl) throws IOException {
        connection = (HttpURLConnection) new URL(sUrl).openConnection();
        connection.connect();
        if (HttpURLConnection.HTTP_OK != connection.getResponseCode()) {
            Log.d(TAG, "Server returned " + connection.getResponseCode()
                    + " " + connection.getResponseMessage());
            return null;
        }
        File file = new File(imgFolder, fileName);
        file.createNewFile();
        Log.d(TAG, String.format("is file [%s] created? %s", file.getAbsolutePath(), file.exists()));
        int fileSize = connection.getContentLength();
        inputStream = connection.getInputStream();
        outputStream = new FileOutputStream(file, false);

        byte[] data = new byte[1024];
        long total = 0;
        int count;
        while ((count = inputStream.read(data)) != -1) {
            if (isCancelled()) {
                inputStream.close();
                return null;
            }
            total += count;
            if (fileSize > 0) {
                publishProgress((int) (total * 100 / fileSize));
            }
            outputStream.write(data, 0, count);
        }
        Log.d(TAG, String.format("Download finished. Total %s bytes", total));
        return file;
    }
}
