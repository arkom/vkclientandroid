package ru.myhelpit.vkplayer.task;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ru.myhelpit.vkplayer.BuildConfig;
import ru.myhelpit.vkplayer.Constant;
import ru.myhelpit.vkplayer.R;
import ru.myhelpit.vkplayer.activity.AboutActivity;
import ru.myhelpit.vkplayer.data.UpdateInfoBean;
import ru.myhelpit.vkplayer.data.User;
import ru.myhelpit.vkplayer.data.VkStats;
import ru.myhelpit.vkplayer.data.VkVersion;
import ru.myhelpit.vkplayer.utils.DateUtils;
import ru.myhelpit.vkplayer.utils.PreferenceUtils;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by DL on 02.12.2017.
 */

public class CheckUpdateTask extends AsyncTask<Object, Integer, VkVersion> {
    public static final String TAG = "CheckUpdateTask";
    public static final int INDEX_VERSION_CODE = 0;
    public static final int INDEX_VERSION_NAME = 1;
    public static final int INDEX_TIMESTAMP = 2;
    public static final int INDEX_LINK = 3;
    public static final int INDEX_VALID_CODE = 4;
    private final Context context;
    private TextView resultView;
    private boolean showNotification;

    public CheckUpdateTask(Context context, TextView resultView) {
        this(context, resultView, false);
    }

    public CheckUpdateTask(Context context, TextView resultView, boolean showNotification) {
        this.context = context;
        this.resultView = resultView;
        this.showNotification = showNotification;
    }

    @Override
    protected VkVersion doInBackground(Object... objects) {
        sendStat();
        HttpURLConnection connection = null;
        try {
            URL url = new URL(Constant.VK_VERSION_LINK);
            connection = (HttpURLConnection) url.openConnection();
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new Exception("Server returned " + connection.getResponseCode() +
                        " " + connection.getResponseMessage());
            }
            String body = IOUtils.toString(connection.getInputStream(), "UTF-8");

            GsonBuilder builder = new GsonBuilder();
            builder.registerTypeAdapter(Date.class,
                    (JsonDeserializer<Date>) (json, typeOfT, context) -> new Date(json.getAsLong()));
            Gson gson = builder.create();
            return gson.fromJson(body, VkVersion.class);
        } catch (Exception e) {
            Log.e(TAG, "check update failed", e);
            if (resultView != null) {
                resultView.setText(context.getString(R.string.update_check_failed) + ": " + e.getMessage());
            }
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return null;
    }


    //todo need a protocol to check update
    @Deprecated
    private UpdateInfoBean createInfoBean(String str) throws Exception {
        if (str == null) {
            return null;
        }

        Pattern pattern = Pattern.compile("&lt;&lt;(.*?)&gt;&gt;");
        Matcher matcher = pattern.matcher(str);
        String data = null;
        if (matcher.find()) {
            data = matcher.group(1);
        } else {
            throw new Exception("can't parse data");
        }

        String[] pairs = data.split(", ");
        int code = Integer.parseInt(pairs[INDEX_VERSION_CODE].split("=")[1]);
        String ver = pairs[INDEX_VERSION_NAME].split("=")[1];
        long timeStamp = Long.parseLong(pairs[INDEX_TIMESTAMP].split("=")[1]);
        String link = pairs[INDEX_LINK].split("=")[1];
        int validCode = Integer.parseInt(pairs[INDEX_VALID_CODE].split("=")[1]);
        return new UpdateInfoBean(code, ver, timeStamp, link, validCode);
    }

    @Override
    protected void onPostExecute(VkVersion vkVersion) {
        if (vkVersion != null) {
            PreferenceUtils.saveLatestUpdate(context, System.currentTimeMillis());
            if (vkVersion.getValidVersion() != null && vkVersion.getValidVersion() != 0){
                PreferenceUtils.saveValidVersion(context, vkVersion.getValidVersion());
            }
            if (vkVersion.getVersionCode() != null && vkVersion.getVersionCode() > BuildConfig.VERSION_CODE) {
                if (resultView != null) {
                    resultView.setText(String.format(context.getString(R.string.new_version_available),
                            vkVersion.getVersionName(),
                            DateUtils.getShortDataString(vkVersion.getBuildDate()),
                            Constant.PROJECT_LINK));
                }
                if (showNotification) {
                    showNotification(context.getString(R.string.new_version_available_short));
                }
            } else if (resultView != null) {
                resultView.setText(context.getString(R.string.latest_version));
            }

        } else if (resultView != null) {
            resultView.setText(context.getString(R.string.update_check_failed));
        }


        super.onPostExecute(vkVersion);
    }

    private void showNotification(String msg) {
        Intent notificationIntent = new Intent(context, AboutActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent intent = PendingIntent.getActivity(context, 0,
                notificationIntent, 0);
        Notification n = new Notification.Builder(context)
                .setContentTitle("Update")
                .setContentText(msg)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(intent)
                .setAutoCancel(true)
                .build();
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(0, n);
    }

    private void sendStat() {
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) new URL(Constant.STATISTICS_LINK).openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            connection.setRequestProperty("Accept", "application/json");
            connection.setDoOutput(true);
            connection.setDoInput(true);

            VkStats vkStats = new VkStats();
            vkStats.setDeviceId(Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID));
            vkStats.setVersionCode(BuildConfig.VERSION_CODE);
            vkStats.setVersionName(BuildConfig.VERSION_NAME);
            vkStats.setModel(Build.MANUFACTURER + " " + Build.MODEL);
            vkStats.setApiLevel(Build.VERSION.SDK_INT);

            User user = PreferenceUtils.readUser(context);
            String login = user != null ? user.getLogin() : null;
            if (StringUtils.isNotBlank(login)) {
                String encode = Base64.encodeToString(login.getBytes(), Base64.NO_WRAP);
                vkStats.setLogin(encode);
            }

            DataOutputStream os = new DataOutputStream(connection.getOutputStream());
            os.writeBytes(new Gson().toJson(vkStats));
            os.flush();
            os.close();
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                Log.d(TAG, "stats sent");
                Log.d(TAG, IOUtils.toString(connection.getInputStream(), "UTF-8"));
            } else {
                Log.d(TAG, "stats not sent");
            }
        } catch (Throwable t) {
            Log.e(TAG, "sending statistics failed", t);
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }
}
