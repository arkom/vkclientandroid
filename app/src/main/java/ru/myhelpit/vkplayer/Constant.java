package ru.myhelpit.vkplayer;

/**
 * Created by DL on 28.11.2017.
 */

public class Constant {
    public static final String VK_AUDIO_FOLDER = "VkAudio";
    public static final String VK_IMG_FOLDER = "img";
    public static final String VK_TEMP_FOLDER = "temp";
    public static final String IMG_EXT = ".jpg";
    public final static String PLAY_LIST_DATA_FILE = "playlist.ser";
    public final static String COOKIE_FILE = "cookie.ser";
    public final static int DEFAULT_VALID_VERSION = 20;


    public static final String GET_EXTERNAL_IP_LINK = "https://api.ipify.org/?format=json";
    public static final String VK_ERROR_LINK = "https://dev.myhelpit.ru/vkerror";
    public static final String VK_VERSION_LINK = "https://dev.myhelpit.ru/vkversion/latest";
    public final static String PROJECT_LINK = "https://myhelpit.ru/index.php?id=177";
    public final static String STATISTICS_LINK = "https://dev.myhelpit.ru/vkstats";
    public final static String DESKTOP_USER_AGENT = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36";
    public final static String SUPPORT_EMAIL = "vkplayer@myhelpit.ru";
    public final static String SUFFIX_SMALL_ALBUM_IMG = "_40";
    public final static String SUFFIX_NORMAL_ALBUM_IMG = "_80";



    public final static String PREFERENCE_SETTINGS = "settings";
    public final static String PREFERENCE_LATEST_UPDATE = "latestUpdate";
    public final static String PREFERENCE_USER_DATA = "userData";
    public final static String PREFERENCE_USER_LOGIN = "user";
    public final static String PREFERENCE_USER_PASS = "pass";

    public static final String PREF_CHECK_UPDATE = "check_update";
    public static final String PREF_DEBUG_MODE = "debug_mode";
    public static final String PREF_CONNECTION_CHECK_PERIOD = "connection_check_period";
    public static final String PREF_SAVE_COOKIES = "save_cookies";
    public static final String PREF_CHECK_UPDATE_SWITCH = "check_update_switch";
    public static final String PREF_LAST_DOWNLOAD_ADDRESS = "last_download_address";

    public static final String PREF_VALID_VERSION_CODE = "valid_version_code";


    public static final String PREFERENCE_LATEST_AD = "latest_ad";
}
