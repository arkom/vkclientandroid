package ru.myhelpit.vkplayer.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import ru.myhelpit.vkplayer.BuildConfig;

/**
 * Created by DL on 02.12.2017.
 */

public class DateUtils {
    private static Date buildDate;
    private static final String SHORT_DATA_FORMAT = "yyyy/MM/dd";

    public static Date getBuildDate(){
        if (buildDate == null){
            buildDate = new Date(BuildConfig.TIMESTAMP);
        }
        return buildDate;
    }

    public static String getShortDataString(long stamp){
        DateFormat format = new SimpleDateFormat(SHORT_DATA_FORMAT);
        return format.format(new Date(stamp));
    }

    public static String getShortDataString(Date date){
        if (date == null){
            return "";
        }
        DateFormat format = new SimpleDateFormat(SHORT_DATA_FORMAT);
        return format.format(date);
    }

    public static boolean moreThanDayPassed(long since, int days) {
        if (since == 0){
            return true;
        }
        Calendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(since);
        calendar.add(Calendar.DAY_OF_YEAR, days);
        return calendar.getTimeInMillis() < System.currentTimeMillis();
    }
}
