package ru.myhelpit.vkplayer.utils;

import android.content.Context;
import android.os.Build;

import org.apache.commons.collections4.CollectionUtils;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.HttpCookie;
import java.net.URI;
import java.util.AbstractMap;
import java.util.List;

import ru.myhelpit.vkclient.utils.BaseSerializableCookie;
import ru.myhelpit.vkclient.utils.HttpCookieSaver;
import ru.myhelpit.vkplayer.Constant;

public class VkCookieSaver extends HttpCookieSaver {
    private Context context;


    public VkCookieSaver(Context context) {
        this.context = context;
        isProcessHttpOnly = Build.VERSION.SDK_INT >= Build.VERSION_CODES.N;
    }

    @Override
    public void write(URI uri, List<HttpCookie> cookies) throws IOException {
        if (CollectionUtils.isEmpty(cookies)){
            return;
        }
        setOut(new ObjectOutputStream(
                context.openFileOutput(Constant.COOKIE_FILE, Context.MODE_PRIVATE)));
        super.write(uri, cookies);
    }

    @Override
    public AbstractMap.SimpleEntry<URI, List<HttpCookie>> read()
            throws IOException, ClassNotFoundException {
        List<BaseSerializableCookie> serializableList = Serializer.readSerializable(context, Constant.COOKIE_FILE);
        return convertToHttpCookie(serializableList);
    }
}
