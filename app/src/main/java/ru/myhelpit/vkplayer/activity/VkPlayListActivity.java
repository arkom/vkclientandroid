package ru.myhelpit.vkplayer.activity;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import ru.myhelpit.vkclient.audio.Record;
import ru.myhelpit.vkclient.controller.VkClient;
import ru.myhelpit.vkplayer.Constant;
import ru.myhelpit.vkplayer.MediaPlayerService;
import ru.myhelpit.vkplayer.R;
import ru.myhelpit.vkplayer.RecordAdapter;
import ru.myhelpit.vkplayer.VkPlayerApplication;
import ru.myhelpit.vkplayer.data.MsgId;
import ru.myhelpit.vkplayer.data.PlayList;
import ru.myhelpit.vkplayer.data.RecordExt;
import ru.myhelpit.vkplayer.data.User;
import ru.myhelpit.vkplayer.task.ConnectionCheckTask;
import ru.myhelpit.vkplayer.task.DownloadFileAsync;
import ru.myhelpit.vkplayer.task.DownloadImageTask;
import ru.myhelpit.vkplayer.task.DownloadPlayListTask;
import ru.myhelpit.vkplayer.task.VkCallback;
import ru.myhelpit.vkplayer.utils.AppUtils;
import ru.myhelpit.vkplayer.utils.Notifier;
import ru.myhelpit.vkplayer.utils.PreferenceUtils;
import ru.myhelpit.vkplayer.utils.Serializer;

import static ru.myhelpit.vkplayer.Constant.IMG_EXT;
import static ru.myhelpit.vkplayer.Constant.PLAY_LIST_DATA_FILE;
import static ru.myhelpit.vkplayer.Constant.SUFFIX_NORMAL_ALBUM_IMG;
import static ru.myhelpit.vkplayer.Constant.SUFFIX_SMALL_ALBUM_IMG;
import static ru.myhelpit.vkplayer.utils.PermissionUtils.verifyStoragePermissions;

public class VkPlayListActivity extends AppCompatActivity implements VkCallback, SwipeRefreshLayout.OnRefreshListener {
    private static final int ITEM_ID_SAVE = 1;
    private static final int ITEM_ID_ABOUT = 3;
    private static final int ITEM_ID_EXIT = 4;
    private static final int ITEM_ID_LOGS = 5;
    private static final int ITEM_ID_SETTINGS = 6;
    private static final int ITEM_ID_SHUFFLE = 7;
    private static final int ITEM_ID_OFFLINE = 8;
    private static final int ITEM_ID_OPEN_SITE = 9;
    private static final int ITEM_ID_TEST = 10;

    private RecordAdapter recordAdapter;
    private PlayList playList;

    private TextView txtInfo;
    private ListView playListView;
    private static final String TAG = "VkPlayListActivity";

    private MediaPlayerService player;
    private boolean mBound = false;
    BroadcastReceiver br;
    private Tracker mTracker;


    private MediaPlayerService.PlayerServiceBinder playerServiceBinder;
    private MediaControllerCompat mediaController;
    private MediaControllerCompat.Callback callback;

    private boolean playing = false;
    private boolean binded;
    private Timer timer;
    private String ip = "";
    private Handler handler;
    private SwipeRefreshLayout refreshLayout;
    private VkClient vkClient;
    private boolean closeApp = false;

    /**
     * Defines callbacks for service binding, passed to bindService()
     */
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {

            playerServiceBinder = (MediaPlayerService.PlayerServiceBinder) service;
            MediaPlayerService mediaService = playerServiceBinder.getInstance();
            mediaService.setHandler(handler);
            try {
                mediaController = new MediaControllerCompat(VkPlayListActivity.this, playerServiceBinder.getMediaSessionToken());
                mediaController.registerCallback(callback);
                callback.onPlaybackStateChanged(mediaController.getPlaybackState());
            } catch (RemoteException e) {
                mediaController = null;
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            playerServiceBinder = null;
            if (mediaController != null) {
                mediaController.unregisterCallback(callback);
                mediaController = null;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, " onCreate*******");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vk_play_list);

        final ImageButton playButton = findViewById(R.id.btnPlayer);

        // Obtain the shared Tracker instance.
        VkPlayerApplication application = (VkPlayerApplication) getApplication();
        mTracker = application.getDefaultTracker();

        verifyStoragePermissions(this);

        Intent intent = getIntent();
        String msg = intent.getStringExtra(LoginActivity.EXTRA_MESSAGE);
        txtInfo = findViewById(R.id.txtInfo);
        txtInfo.setText(msg);

        playList = PlayList.getInstance();
        boolean fromCache = intent.getBooleanExtra(LoginActivity.EXTRA_FROM_CACHE, false);

        playListView = findViewById(R.id.playListView);
        playListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (playListView.getChildAt(0) != null) {
                    refreshLayout.setEnabled(playListView.getFirstVisiblePosition() == 0 && playListView.getChildAt(0).getTop() == 0);
                }
            }
        });
        if (playList != null) {
            playListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
            recordAdapter = new RecordAdapter(this, playList);
            playListView.setAdapter(recordAdapter);
            syncWithLocalSources();
            if (!fromCache) {
                downloadImages();
            }
        }

        callback = new MediaControllerCompat.Callback() {
            @Override
            public void onSessionEvent(String event, Bundle extras) {
                Log.d(TAG, "onSessionEvent");
                super.onSessionEvent(event, extras);
            }

            @Override
            public void onPlaybackStateChanged(PlaybackStateCompat state) {
                Log.i(TAG, "onPlaybackStateChanged");
                Log.i(TAG, "state: " + state);
                Log.i(TAG, "state: " + state);
                if (state == null)
                    return;
                playing = state.getState() == PlaybackStateCompat.STATE_PLAYING;
                playButton.setImageResource(playing ?
                        R.drawable.ic_action_pause :
                        R.drawable.ic_action_play);

                if (recordAdapter != null) {
                    recordAdapter.notifyDataSetChanged();
                }
            }
        };

        binded = bindService(new Intent(this, MediaPlayerService.class), serviceConnection, BIND_AUTO_CREATE);
        Log.d(TAG, "service is binded: " + binded);

        handler = new PlayListHandler();

        ConnectionCheckTask task = new ConnectionCheckTask(handler);
        timer = new Timer();
        int period = PreferenceUtils.getConnectionCheckPeriod(getApplication());
        timer.schedule(task, 1, period * 1000);

        refreshLayout = findViewById(R.id.swiperefresh);
        refreshLayout.setOnRefreshListener(this);

        vkClient = ((VkPlayerApplication)getApplication()).getVkClient();

        if (PreferenceUtils.moreThanDayAfterShowAd(getApplicationContext())) {
            AppUtils.showProjectPageDialog(this);
            PreferenceUtils.saveLatestShowAd(getApplicationContext(), System.currentTimeMillis());
        }
    }


    private void save() {
        if (playList != null) {
            Serializer.saveSerializable(getApplicationContext(), playList.getPlaylistForSaving(), PLAY_LIST_DATA_FILE);
            Notifier.show(getApplication(), getString(R.string.playlist_saved));
        }
    }

    //it doesn't work todo fix it
    @Deprecated
    public void reloadPlayList(View view) {
        ArrayList<RecordExt> records = playList.getSelectedItems();
        if (records.isEmpty()) {
            return;
        }
        Map<String, Record> toReload = new HashMap<>();
        for (RecordExt rec : records) {
            toReload.put(rec.getVkId(), rec.getRecord());
        }

        //only for test. remove login from here!!!
        if (!vkClient.isLogin()) {
            try {
                vkClient.login("", "");
            } catch (Exception e) {
                Log.e(TAG, "login failed", e);
                return;
            }
        }

        if (vkClient.isLogin()) {
            try {
                vkClient.reloadRecords(toReload);
                Notifier.show(getApplication(), "playlist is updated successful");
            } catch (Exception e) {
                String msg = "playlist not updated!"; //get it from resource
                Log.e(TAG, msg, e);
                Notifier.show(getApplication(), msg, e);
            }
        } else {
            Notifier.show(getApplication(), "playlist not updated! you need to login on VK");
        }
    }

    public void load(View view) {
        ArrayList<RecordExt> records = playList.getSelectedItems();
        if (records.isEmpty()) {
            return;
        }
        ProgressBar progressBar = findViewById(R.id.progressBar);
        for (RecordExt record : records) {
            DownloadFileAsync fileAsync =
                    new DownloadFileAsync(getApplicationContext(), progressBar, this);
            fileAsync.execute(record);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, ITEM_ID_SHUFFLE, 0, R.string.shuffle);
        menu.add(0, ITEM_ID_SAVE, 0, R.string.save_playlist);
        menu.add(0, ITEM_ID_ABOUT, 0, R.string.about);
        menu.add(0, ITEM_ID_LOGS, 0, R.string.logs);
        MenuItem item = menu.add(0, ITEM_ID_SETTINGS, 0, getString(R.string.title_activity_settings));
        item.setIntent(new Intent(this, SettingsActivity.class));
        MenuItem offline = menu.add(0, ITEM_ID_OFFLINE, 0, R.string.offline_mode);
//        offline.setActionView(R.layout.switch_layout);
        menu.add(0, ITEM_ID_OPEN_SITE, 0, getString(R.string.open_site));
        menu.add(0, ITEM_ID_EXIT, 0, R.string.exit);
//        menu.add(0, ITEM_ID_TEST, 0, "test error");
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case ITEM_ID_SAVE:
                save();
                break;
            case ITEM_ID_ABOUT:
                Intent intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
                break;
            case ITEM_ID_EXIT:
                if (recordAdapter != null) {
                    playList.markStoppedCurrent();
                }
                save();
                unbindMediaService();
                closeApp = true;
                finish();
                break;
            case ITEM_ID_LOGS:
                Intent logIntent = new Intent(this, LogsActivity.class);
                startActivity(logIntent);
                break;
            case ITEM_ID_SHUFFLE:
                boolean checked = !item.isChecked();
                item.setChecked(checked);
                item.setTitle(checked ? R.string.restore_order : R.string.shuffle);
                if (mediaController != null) {
                    mediaController.getTransportControls().stop();
                }
                playList.shuffle(checked);
                recordAdapter.notifyDataSetChanged();
                break;
            case ITEM_ID_OFFLINE:
                boolean isChecked = !item.isChecked();
                item.setChecked(isChecked);
                item.setTitle(isChecked ? R.string.online_mode : R.string.offline_mode);
                playList.setOfflineMode(isChecked);
                break;
            case ITEM_ID_OPEN_SITE:
                AppUtils.showProjectPageDialog(this);
                break;
            case ITEM_ID_TEST:
                throw new IllegalArgumentException("test");

        }
        return super.onOptionsItemSelected(item);
    }

    private void syncWithLocalSources() {
        SynchronizeLocalSource task = new SynchronizeLocalSource();
        task.execute();
    }

    private void downloadImages() {
        DownloadImageTask downloadImageTask = new DownloadImageTask(getApplicationContext(), this);
        downloadImageTask.execute(playList.getAllRecords());
    }


    public void play(View view) {
        int position = (Integer) view.getTag();

        if (playing) {
            mediaController.getTransportControls().pause();
            if (!playList.isCurrentItemId(position) && applyCurrentItemId(position)) {
                mediaController.getTransportControls().play();
            }
        } else {
            if (!playList.isCurrentItemId(position) && !applyCurrentItemId(position)) {
                return;
            }
            mediaController.getTransportControls().play();
        }
    }

    private boolean applyCurrentItemId(int position) {
        RecordExt track = playList.getByIndex(position);
        if (track == null){
            return false;
        }
        if (StringUtils.isEmpty(track.getUrl()) && StringUtils.isEmpty(track.getFilePath())) {
            return false;
        }
        playList.setCurrentItemId(position);
        return true;
    }

    public void playerControls(View view) {
        if (mediaController == null) {
            Log.w(TAG, "mediaController is null!!!");
            return;
        }
        switch (view.getId()) {
            case R.id.btnBackward:
                mediaController.getTransportControls().skipToPrevious();
                break;
            case R.id.btnForward:
                mediaController.getTransportControls().skipToNext();
                break;
            case R.id.btnPlayer:
                if (playing) {
                    mediaController.getTransportControls().pause();
                } else {
                    mediaController.getTransportControls().play();
                }
                break;
        }
    }

    /**
     * the hardware back button was pressed for exit
     */
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle("Выйти из приложения?")
                .setMessage("Вы действительно хотите выйти?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        //SomeActivity - имя класса Activity для которой переопределяем onBackPressed();
                        save();
                        unbindMediaService();
                        closeApp = true;
                        VkPlayListActivity.super.onBackPressed();
                    }

                }).create().show();
    }

    private void unbindMediaService() {
        if (binded) {
            stopService(new Intent(this, MediaPlayerService.class));
            binded = false;
        }
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy");
        playerServiceBinder = null;
        if (mediaController != null) {
            mediaController.unregisterCallback(callback);
            mediaController = null;
        }
        unbindService(serviceConnection);

        if (timer != null){
            timer.cancel();
        }
        super.onDestroy();
        Log.d(TAG, "onDestroy after super");
        if (closeApp) {
            System.exit(0);
        }
    }

    @Override
    public void callback() {
        if (recordAdapter != null) {
            recordAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onResume() {
        Log.i(TAG, "Setting screen name: " + TAG);
        mTracker.setScreenName("Image~" + TAG);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        super.onResume();
    }

    @Override
    public void onRefresh() {
        updatePlayList();
    }

    private void updatePlayList() {
        User user = PreferenceUtils.readUser(getApplication());
        if (user == null){
            Notifier.show(getApplicationContext(), "no saved user!");
            refreshLayout.setRefreshing(false);
        }

        DownloadPlayListTask task = new DownloadPlayListTask(
                vkClient, user, handler, getApplicationContext());
        task.execute();
    }

    private class SynchronizeLocalSource extends AsyncTask<Object, Integer, Object> {
        @Override
        protected void onPostExecute(Object o) {
            recordAdapter.notifyDataSetChanged();
            super.onPostExecute(o);
        }

        @Override
        protected Object doInBackground(Object... objects) {
            final File audioFolder = new File(Environment.getExternalStorageDirectory(), Constant.VK_AUDIO_FOLDER);
            if (!audioFolder.exists() || !audioFolder.isDirectory()
                    || audioFolder.listFiles() == null || audioFolder.listFiles().length < 1) {
                return "audio folder doesn't exist!";
            }
            final File imgFolder = new File(audioFolder, Constant.VK_IMG_FOLDER);

            syncAudio(audioFolder);
            syncImages(imgFolder);
            return null;
        }

        private void syncAudio(File audioFolder) {
            for (RecordExt record : playList.getAllRecords()) {
                if (record.getRecordName() == null) {
                    continue;
                }
                boolean found = false;
                for (File file : audioFolder.listFiles()) {
                    if (file.getName().equals(record.getRecordName())) {
                        found = true;
                        record.setFilePath(file.getAbsolutePath());
                        break;
                    }
                }
                if (!found) {
                    record.setFilePath(null);
                }
            }
        }

        private void syncImages(File imgFolder) {
            if (imgFolder == null ||
                    !imgFolder.isDirectory() ||
                    !imgFolder.exists() ||
                    imgFolder.listFiles() == null) {
                Log.w(TAG, "img folder doesn't exist");
                return;
            }
            for (RecordExt record : playList.getAllRecords()) {
                boolean img40found = false;
                boolean img80found = false;
                for (File file : imgFolder.listFiles()) {
                    if (file.getName().equals(record.getVkId() + SUFFIX_SMALL_ALBUM_IMG + IMG_EXT)) {
                        img40found = true;
                        record.setImage40x40Path(file.getAbsolutePath());
                    }
                    if (file.getName().equals(record.getVkId() + SUFFIX_NORMAL_ALBUM_IMG + IMG_EXT)) {
                        img80found = true;
                        record.setImage80x80Path(file.getAbsolutePath());
                    }
                    if (img40found && img80found) {
                        break;
                    }
                }
                if (!img40found) {
                    record.setImage40x40Path(null);
                }
                if (!img80found) {
                    record.setImage80x80Path(null);
                }
            }
        }
    }

    private class PlayListHandler extends Handler{
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MsgId.OFFLINE:
                    txtInfo.setText("OFFLINE");
                    txtInfo.setBackgroundResource(R.color.colorRed200);
                    break;
                case MsgId.ONLINE:
                    ip = (String) msg.obj;
                    showIp(ip);
                    break;
                case MsgId.START:
                    Notifier.show(getApplication(), "updating...");
                    break;
                case MsgId.FINISH:
                    String result = (String) msg.obj;
                    if (result == null) {
                        Notifier.show(getApplicationContext(), "play list updated!");
                        showIp(ip);
                    } else {
                        Notifier.showLong(getApplication(), result);
                    }
                    SynchronizeLocalSource task = new SynchronizeLocalSource();
                    task.execute();
                    refreshLayout.setRefreshing(false);
                    recordAdapter.notifyDataSetChanged();
                    break;
                case MsgId.PLAYER_INVALID_RESPONSE:
                    String s = (String) msg.obj;
                    if (s == null) {
                        Notifier.showLong(getApplicationContext(), getString(R.string.player_invalid_response));
                        break;
                    }
                case MsgId.DATASOURCE_EXCEPTION:
                case MsgId.OTHER_PLAYER_EXCEPTION:
                    Notifier.showLong(getApplicationContext(), (String)msg.obj);
                    break;
                case MsgId.TRACK_CHANGED:
                    Integer id = playList.getCurrentItemId();
                    if (id != null) {
                        playListView.smoothScrollToPosition(id);
                    }
                    break;
                default:
                    txtInfo.setText("received an unknown message: "+msg.what);
                    txtInfo.setBackgroundResource(R.color.colorGrey200);
            }
        }

        private void showIp(String ip) {
            String text = "ONLINE|IP: " + ip;
            if (!ip.equals(PreferenceUtils.readLastDownloadAddress(getApplicationContext()))){
                text += "| плейлист загружен с другого IP. Необходимо обновить плейлист";
            }

            txtInfo.setText(text);
            txtInfo.setBackgroundResource(R.color.colorGreen);
        }
    }
}
