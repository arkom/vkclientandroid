package ru.myhelpit.vkplayer.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import ru.myhelpit.vkplayer.Constant;

/**
 * Created by DL on 06.12.2017.
 */

public final class MyAppLogReader {

    private static final String TAG = MyAppLogReader.class.getCanonicalName();
    private static final String processId = Integer.toString(android.os.Process
            .myPid());

    public static StringBuilder getLog() {

        StringBuilder builder = new StringBuilder();

        try {
            String[] command = new String[]{"logcat", "-d", "-v", "threadtime"};

            Process process = Runtime.getRuntime().exec(command);

            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(process.getInputStream()));

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                if (line.contains(processId)) {
                    builder.append(line).append("\n");
                }
            }
        } catch (IOException ex) {
            Log.e(TAG, "getLog failed", ex);
        }

        return builder;
    }

    public static void sendLog(Context context) {
        StringBuilder log = getLog();
        if (log.length() > 0) {
            Intent intent = new Intent(Intent.ACTION_SENDTO); // it's not ACTION_SEND
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_SUBJECT, "logs of VkPlayer");
            intent.putExtra(Intent.EXTRA_TEXT, log.toString());
            intent.setData(Uri.parse("mailto:" + Constant.SUPPORT_EMAIL));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
            context.startActivity(intent);
        } else {
            Notifier.show(context, "no logs to be sent");
            Log.d(TAG, "no logs to be sent");
        }
    }
}
