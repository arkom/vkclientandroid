package ru.myhelpit.vkplayer.data;

public class MsgId {
    public static final int UNKNOWN = -1;
    public static final int OFFLINE = 0;
    public static final int ONLINE = 1;
    public static final int START = 2;
    public static final int FINISH = 3;
    public static final int LOGIN_BY_PASS = 4;
    public static final int PLAYER_INVALID_RESPONSE = 5;
    public static final int DATASOURCE_EXCEPTION = 6;
    public static final int OTHER_PLAYER_EXCEPTION = 7;
    public static final int TRACK_CHANGED = 8;
}
