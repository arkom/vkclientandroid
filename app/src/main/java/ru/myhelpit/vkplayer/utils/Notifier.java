package ru.myhelpit.vkplayer.utils;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by DL on 26.11.2017.
 */

/**
 * simple pop-up notification
 */
public class Notifier {
    public static void show(Context ctx, String msg) {
        show(ctx, msg, null);
    }

    public static void show(Context ctx, String msg, Exception e) {
        String str = e != null ? msg + " " + e.getMessage() : msg;
        Toast toast = Toast.makeText(ctx, str, Toast.LENGTH_SHORT);
        toast.show();
    }

    public static void showLong(Context ctx, String msg) {
        showLong(ctx, msg, null);
    }

    public static void showLong(Context ctx, String msg, Exception e) {
        String str = e != null ? msg + " " + e.getMessage() : msg;
        Toast toast = Toast.makeText(ctx, str, Toast.LENGTH_LONG);
        toast.show();
    }
}
