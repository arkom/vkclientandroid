package ru.myhelpit.vkplayer.utils;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.gson.Gson;

import java.io.DataOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import ru.myhelpit.vkplayer.BuildConfig;
import ru.myhelpit.vkplayer.Constant;
import ru.myhelpit.vkplayer.R;
import ru.myhelpit.vkplayer.data.VkError;

public class AppUtils {
    private static String TAG = "AppUtils";

    public static void showProjectPageDialog(AppCompatActivity activity) {
        AlertDialog.Builder alert = new AlertDialog.Builder(activity);
        alert.setTitle(activity.getString(R.string.project_page_dialog_title));

        WebView wv = new WebView(activity);
        WebSettings webSettings = wv.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setUserAgentString(Constant.DESKTOP_USER_AGENT);

        wv.loadUrl(Constant.PROJECT_LINK);
        wv.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });

        alert.setView(wv);
        alert.setNegativeButton("Close", (dialog, id) -> dialog.dismiss());
        alert.show();
    }

    public static String getStackTraceAsString(Throwable throwable) {
        Writer writer = new StringWriter();
        PrintWriter printWriter = new PrintWriter(writer);
        throwable.printStackTrace(printWriter);
        return writer.toString();
    }

    public static boolean sendReport(String report, Throwable throwable, Context context, String comment) {
        VkError error = new VkError();
        error.setDeviceId(Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID));
        error.setVersionCode(BuildConfig.VERSION_CODE);
        error.setVersionName(BuildConfig.VERSION_NAME);
        error.setModel(Build.MANUFACTURER + " " + Build.MODEL);
        error.setTrace(report);
        error.setComment(comment != null ? comment : throwable.getMessage());

        Gson gson = new Gson();
        Log.i(TAG, "json: " + gson.toJson(error));

        try {
            URL url = new URL(Constant.VK_ERROR_LINK);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            conn.setRequestProperty("Accept", "application/json");
            conn.setDoOutput(true);
            conn.setDoInput(true);

            DataOutputStream os = new DataOutputStream(conn.getOutputStream());
            os.writeBytes(gson.toJson(error));
            os.flush();
            os.close();

            Log.i("STATUS", String.valueOf(conn.getResponseCode()));

            conn.disconnect();
        } catch (Exception e) {
            Log.e(TAG, "can't send error to server", e);
            return false;
        }
        return true;
    }

    public static boolean sendReport(Throwable throwable, Context context, String comment) {
        return sendReport(getStackTraceAsString(throwable), throwable, context, comment);
    }
}
