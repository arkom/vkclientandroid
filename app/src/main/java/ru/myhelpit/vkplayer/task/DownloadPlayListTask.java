package ru.myhelpit.vkplayer.task;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import org.apache.commons.collections4.MapUtils;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Map;

import ru.myhelpit.vkclient.audio.Record;
import ru.myhelpit.vkclient.controller.VkClient;
import ru.myhelpit.vkplayer.Constant;
import ru.myhelpit.vkplayer.R;
import ru.myhelpit.vkplayer.data.MsgId;
import ru.myhelpit.vkplayer.data.PlayList;
import ru.myhelpit.vkplayer.data.User;
import ru.myhelpit.vkplayer.utils.AppUtils;
import ru.myhelpit.vkplayer.utils.PreferenceUtils;
import ru.myhelpit.vkplayer.utils.Serializer;

public class DownloadPlayListTask extends AsyncTask<Object, String, String> {
    public static final String TAG = "DownloadPlayListTask";
    private final User user;

    private final VkClient vkClient;

    private Handler handler;
    private Context context;

    public DownloadPlayListTask(VkClient vkClient, User user, Handler handler, Context context) {
//        Objects.requireNonNull(user, "user must not be null"); todo need to check?
        this.user = user;
        this.handler = handler;
        this.context = context;
        this.vkClient = vkClient;
    }

    @Override
    protected void onPreExecute() {
        handler.sendEmptyMessage(MsgId.START);
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String result) {
        handler.sendMessage(handler.obtainMessage(MsgId.FINISH, result));
        super.onPostExecute(result);
    }

    @Override
    protected String doInBackground(Object... objects) {
        try {
            vkClient.restoreCookie(null);
            boolean isValidCookies = vkClient.checkLogin();
            boolean isLogin = false;
            //todo does need to check user?
            if (!isValidCookies && user != null) {
                isLogin = doLogin(user.getLogin(), user.getPass());
            }
            if (isLogin) {
                handler.sendMessage(handler.obtainMessage(MsgId.LOGIN_BY_PASS, user));
            }
            if (isValidCookies || isLogin) {
                PlayList playList = PlayList.getInstance();
                Map<String, Record> map = vkClient.getPlayList();
                if (playList.isShuffle()) {
                    playList.updateUrlOrPut(map);
                } else if (MapUtils.isNotEmpty(map)) {
                    playList.recreate(map);
                    Serializer.saveSerializable(context, playList.getPlaylistForSaving(), Constant.PLAY_LIST_DATA_FILE);
                }
                ConnectionCheckTask checkTask = new ConnectionCheckTask(handler);
                String ip = checkTask.getIp();
                Log.d(TAG, ip);
                PreferenceUtils.saveLastDownloadAddress(context, ip);
            } else {
                return context.getString(R.string.login_failed);
            }
        } catch (Exception e) {
            Log.e(TAG, context.getString(R.string.get_playlist_error), e);
            AppUtils.sendReport(e, context, "download playlist error");
            return context.getString(R.string.login_failed) + " " + e.getMessage();
        }
        return null;
    }

    private boolean doLogin(String login, String pass) throws IOException, URISyntaxException {
        boolean result = false;
        if (!vkClient.isLogin()) {
            result = vkClient.login(login, pass);
        }
        return result;
    }

}
