package ru.myhelpit.vkplayer.data;

import java.io.Serializable;

import ru.myhelpit.vkclient.audio.Record;

/**
 * Created by DL on 26.11.2017.
 */

public class RecordExt implements Serializable {
    private Record record;
    transient private boolean selected;
    transient private boolean play;

    private String originalRecordName;
    private String recordName;
    private String filePath;
    private String image40x40Path;
    private String image80x80Path;

    public RecordExt(Record record) {
        this.record = record;
    }

    public int getUserId() {
        return record.getUserId();
    }

    public int getAudioId() {
        return record.getAudioId();
    }

    public String getName() {
        return record.getName();
    }

    public void setName(String name) {
        record.setName(name);
    }

    public String getExecutor() {
        return record.getExecutor();
    }

    public void setExecutor(String executor) {
        record.setExecutor(executor);
    }

    public long getDuration() {
        return record.getDuration();
    }

    public void setDuration(long duration) {
        record.setDuration(duration);
    }

    public String getUrl() {
        return record.getUrl();
    }

    public void setUrl(String url) {
        record.setUrl(url);
    }

    public String getVkId() {
        return record.getVkId();
    }

    public boolean isPlay() {
        return play;
    }

    public void setPlay(boolean play) {
        this.play = play;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public Record getRecord() {
        return record;
    }

    public String getRecordName() {
        return recordName != null ? recordName : getVkId();
    }

    public void setRecordName(String recordName) {
        this.recordName = recordName;
    }

    public String getImg40x40Url() {
        return record.getImg40x40Url();
    }

    public void setImg40x40Url(String img40x40Url) {
        record.setImg40x40Url(img40x40Url);
    }

    public String getImg80x80Url() {
        return record.getImg80x80Url();
    }

    public void setImg80x80Url(String img80x80Url) {
        record.setImg80x80Url(img80x80Url);
    }

    public String getImage40x40Path() {
        return image40x40Path;
    }

    public void setImage40x40Path(String image40x40Path) {
        this.image40x40Path = image40x40Path;
    }

    public String getImage80x80Path() {
        return image80x80Path;
    }

    public void setImage80x80Path(String image80x80Path) {
        this.image80x80Path = image80x80Path;
    }

    /**
     *
     * @return - the absolute path to the record file
     */
    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public boolean isM3u8(){
        return record.getUrl() != null && record.getUrl().toLowerCase().endsWith(".m3u8");
    }

    public boolean isMp3(){
        return record.getUrl() != null && record.getUrl().toLowerCase().endsWith(".mp3");
    }

    @Override
    public String toString() {
        return "RecordExt{" +
                "record=" + record +
//                ", selected=" + selected +
//                ", play=" + play +
                ", originalRecordName='" + originalRecordName + '\'' +
                ", recordName='" + recordName + '\'' +
                ", filePath='" + filePath + '\'' +
                '}';
    }
}
