package ru.myhelpit.vkplayer.data;

import android.util.Log;

import org.apache.commons.collections4.MapUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import ru.myhelpit.vkclient.audio.Record;

/**
 * Created by DL on 25.11.2017.
 */

public class PlayList implements Serializable {
    private static final String TAG = "PlayList";
    private static PlayList instance = null;
    private Integer currentItemId = null;
    private boolean shuffle = false;
    private Map<String, RecordExt> playList = new LinkedHashMap<>();
    private Map<String, RecordExt> originalPlayList;
    private boolean isOfflineMode = false;

    private PlayList() {
    }

    public static PlayList getInstance() {
        if (instance == null) {
            synchronized (PlayList.class) {
                if (instance == null) {
                    instance = new PlayList();
                }
            }
        }
        return instance;
    }

    /**
     * The method updates url if a record exists otherwise put a new RecordExt
     * @param map
     */
    synchronized public void updateUrlOrPut(Map<String, Record> map) {
        if (MapUtils.isEmpty(map)) {
            return;
        }
        for (String key : map.keySet()) {
            RecordExt recordExt = playList.get(key);
            if (recordExt == null) {
                playList.put(key, new RecordExt(map.get(key)));
            } else {
                recordExt.setUrl(map.get(key).getUrl());
            }
        }
    }

    synchronized public void recreate(Map<String, Record> map) {
        if (MapUtils.isEmpty(map)) {
            return;
        }
        playList.clear();
        for (String key : map.keySet()) {
            playList.put(key, new RecordExt(map.get(key)));
        }
    }

    synchronized public void putAllRecordExt(Map<String, RecordExt> map) {
        if (MapUtils.isEmpty(map)) {
            return;
        }
        playList.putAll(map);
    }

    synchronized public Collection<RecordExt> getAllRecords() {
        return new ArrayList<>(playList.values());
    }

    synchronized public LinkedHashMap<String, RecordExt> getPlaylistAsMap() {
        return new LinkedHashMap<>(playList);
    }

    synchronized public LinkedHashMap<String, RecordExt> getPlaylistForSaving() {
        return new LinkedHashMap<>(originalPlayList == null ? playList: originalPlayList);
    }

    synchronized public boolean isEmpty() {
        return MapUtils.isEmpty(playList);
    }

    synchronized public int size() {
        return playList.size();
    }

    synchronized public RecordExt getByIndex(int i) {
        return (RecordExt) playList.values().toArray()[i];
    }

    synchronized public ArrayList<RecordExt> getSelectedItems() {
        ArrayList<RecordExt> selectedItems = new ArrayList<>();
        for (RecordExt record : playList.values()) {
            if (record.isSelected()) {
                selectedItems.add(record);
            }
        }
        return selectedItems;
    }

    /**
     * get the next record id which has already download
     *
     * @return
     */
    synchronized public RecordExt getNextTrack() {
        markStoppedCurrent();
        int i = currentItemId == null ? 0 : currentItemId + 1;
        for (; i < playList.size(); i++) {
            RecordExt recordExt = getByIndex(i);
            if (recordExt.getFilePath() != null || !isOfflineMode && recordExt.getUrl() != null) {
                currentItemId = i;
                return recordExt;
            }
        }
        return null;
    }

    /**
     * get the previous track id which was downloaded
     *
     * @return
     */
    synchronized public RecordExt getPrevTrack() {
//        todo use stream
        markStoppedCurrent();
        int i = currentItemId == null ? 0 : currentItemId - 1;
        for (; i >= 0; i--) {
            RecordExt recordExt = getByIndex(i);
            if (recordExt.getFilePath() != null || !isOfflineMode && recordExt.getUrl() != null) {
                currentItemId = i;
                return recordExt;
            }
        }
        return null;
    }

    synchronized public RecordExt getCurrentTrack(){
        return currentItemId != null ? getByIndex(currentItemId) : getNextTrack();
    }

    synchronized public void setCurrentItemId(Integer currentItemId) {
        markStoppedCurrent();
        this.currentItemId = currentItemId;
    }


    synchronized public void markStoppedCurrent() {
        if (currentItemId != null) {
            (getByIndex(currentItemId)).setPlay(false);
        } else {
            Log.w(TAG, "currentItemId is null");
        }
    }

    synchronized public boolean isCurrentItemId(Integer position) {
        return currentItemId != null && currentItemId.equals(position);
    }

    synchronized public Integer getCurrentItemId() {
        return currentItemId;
    }

    synchronized public boolean isShuffle() {
        return shuffle;
    }

    synchronized public void shuffle(boolean shuffle) {
        if (this.shuffle == shuffle) {
            return;
        }
        this.shuffle = shuffle;
        markStoppedCurrent();
        if (shuffle) {
            originalPlayList = new LinkedHashMap<>();
            originalPlayList.putAll(playList);
            List<String> keys = new ArrayList<>(playList.keySet());
            Collections.shuffle(keys);
            playList.clear();
            for (String key : keys) {
                playList.put(key, originalPlayList.get(key));
            }
        } else {
            playList.clear();
            playList.putAll(originalPlayList);
            originalPlayList = null;
        }
    }

    synchronized public void clear(){
        playList.clear();
    }

    public void setOfflineMode(boolean value) {
        this.isOfflineMode = value;
    }
}
