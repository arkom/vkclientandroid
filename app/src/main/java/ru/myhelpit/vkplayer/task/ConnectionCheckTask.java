package ru.myhelpit.vkplayer.task;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.TimerTask;

import ru.myhelpit.vkplayer.data.MsgId;

import static ru.myhelpit.vkplayer.Constant.GET_EXTERNAL_IP_LINK;

public class ConnectionCheckTask extends TimerTask {
    private static final String TAG = ConnectionCheckTask.class.getCanonicalName();
    private final Handler handler;


    public ConnectionCheckTask(Handler handler) {
        this.handler = handler;
    }

    @Override
    public void run() {
        String ip = getIp();
        if (handler != null) {
            if (StringUtils.isNotBlank(ip)) {
                Message message = handler.obtainMessage(MsgId.ONLINE, ip);
                handler.sendMessage(message);
            } else {
                handler.sendEmptyMessage(MsgId.OFFLINE);
            }
        }
    }

    public String getIp() {
        String ip = "";
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) new URL(GET_EXTERNAL_IP_LINK).openConnection();
            if (HttpURLConnection.HTTP_OK != connection.getResponseCode()) {
                Log.i(TAG, "server returned code: " + connection.getResponseCode());
                return ip;
            }
            JSONObject jsonObject = new JSONObject(IOUtils.toString(connection.getInputStream(), "UTF-8"));
            String ipAddress = jsonObject.getString("ip");
            if (StringUtils.isNotBlank(ipAddress)) {
                ip = ipAddress;
                Log.i(TAG, "ip address: " + ipAddress);
            }
        } catch (Exception e) {
            Log.e(TAG, "error when trying to get ip", e);
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return ip;
    }
}
