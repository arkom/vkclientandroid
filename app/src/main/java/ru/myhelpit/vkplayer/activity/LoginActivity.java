package ru.myhelpit.vkplayer.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.LinkedHashMap;

import androidx.appcompat.app.AppCompatActivity;
import ru.myhelpit.vkclient.controller.VkClient;
import ru.myhelpit.vkplayer.BuildConfig;
import ru.myhelpit.vkplayer.Constant;
import ru.myhelpit.vkplayer.R;
import ru.myhelpit.vkplayer.VkPlayerApplication;
import ru.myhelpit.vkplayer.data.MsgId;
import ru.myhelpit.vkplayer.data.PlayList;
import ru.myhelpit.vkplayer.data.RecordExt;
import ru.myhelpit.vkplayer.data.User;
import ru.myhelpit.vkplayer.task.CheckUpdateTask;
import ru.myhelpit.vkplayer.task.DownloadPlayListTask;
import ru.myhelpit.vkplayer.utils.PreferenceUtils;
import ru.myhelpit.vkplayer.utils.Serializer;

public class LoginActivity extends AppCompatActivity {
    public final static String EXTRA_MESSAGE = "ru.myhelpit.vkplayer.MESSAGE";
    public final static String EXTRA_FROM_CACHE = "ru.myhelpit.vkplayer.FROM_CACHE";

    private static final String TAG = LoginActivity.class.getCanonicalName();
    private static final int ITEM_ID_LOGS = 0;

    private Intent intent;
    private Tracker mTracker;
    private ProgressDialog dialog;
    private VkClient vkClient;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Log.i(TAG, "deviceId: " + Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID));
        // Obtain the shared Tracker instance.
        VkPlayerApplication application = (VkPlayerApplication) getApplication();
        vkClient = application.getVkClient();
        mTracker = application.getDefaultTracker();
        handler = new LoginHandler();

        checkUpdate();
        PlayList playList = PlayList.getInstance();
        LinkedHashMap<String, RecordExt> data = Serializer.readSerializable(getApplicationContext(), Constant.PLAY_LIST_DATA_FILE);
        if (data != null) {
            playList.putAllRecordExt(data);
        }
        User user = PreferenceUtils.readUser(getApplication());
        intent = new Intent(this, VkPlayListActivity.class);
        if (!isValidVersion()) {
            //show alert with update link and exit
            showUpdateAlert();
            return;
        }
        if (!playList.isEmpty()) {
            intent.putExtra(EXTRA_FROM_CACHE, true); //todo use transient field?
            startActivity(intent);
            finish();
            return;
        }

        //try auto-login
        if (user != null) {
            DownloadPlayListTask downloadPlaylist = new DownloadPlayListTask(
                    vkClient,
                    user,
                    handler,
                    getApplicationContext());
            downloadPlaylist.execute();
        } else {
            Log.i(TAG, "user is null");
        }

    }

    private void showUpdateAlert() {
        TextView message = new TextView(this);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        SpannableString msgText = new SpannableString(getString(R.string.alert_update_message));
        Linkify.addLinks(msgText, Linkify.WEB_URLS);
        message.setText(msgText);
        message.setMovementMethod(LinkMovementMethod.getInstance());
        builder.setTitle("Update Vkplayer")
                .setView(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        System.exit(0);
                    }
                });
        builder.create().show();
    }

    private boolean isValidVersion() {
        int validVersion = PreferenceUtils.readValidVersion(this);
        return validVersion <= BuildConfig.VERSION_CODE;
    }

    private void checkUpdate() {
        if (!PreferenceUtils.isNeedCheckUpdate(getApplication()) && isValidVersion()) {
            Log.i(TAG, "update check skipped");
            return;
        }
        CheckUpdateTask updateTask = new CheckUpdateTask(getApplicationContext(), null, true);
        updateTask.execute();
    }

    public void login(View view) {
        closeVirtualKeyboard();
        String login = ((EditText) findViewById(R.id.fieldLogin)).getText().toString();
        String pass = ((EditText) findViewById(R.id.fieldPass)).getText().toString();
        DownloadPlayListTask downloadPlaylist = new DownloadPlayListTask(
                vkClient, new User(login, pass), handler, getApplicationContext());
        downloadPlaylist.execute();
    }

    private void closeVirtualKeyboard() {
        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        if (getCurrentFocus() != null) {
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    private void saveUserData(String login, String pass) {
        SharedPreferences sPref = getSharedPreferences(Constant.PREFERENCE_USER_DATA, MODE_PRIVATE);
        SharedPreferences.Editor editor = sPref.edit();
        editor.putString(Constant.PREFERENCE_USER_LOGIN, login);
        editor.putString(Constant.PREFERENCE_USER_PASS, pass);
        editor.commit();
    }

    @Override
    protected void onResume() {
        Log.i(TAG, "Setting screen name: " + TAG);
        mTracker.setScreenName("Image~" + TAG);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        super.onResume();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, ITEM_ID_LOGS, 0, "logs");
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case ITEM_ID_LOGS:
                Intent logIntent = new Intent(this, LogsActivity.class);
                startActivity(logIntent);
        }
        return super.onOptionsItemSelected(item);
    }

    private class LoginHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MsgId.START:
                    dialog = ProgressDialog.show(LoginActivity.this, getString(R.string.entering),
                            getString(R.string.loading_please_wait), true);
                    break;
                case MsgId.FINISH:
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    String result = (String) msg.obj;
                    if (result == null) {
                        startActivity(intent);
                        finish();
                    } else {
                        ((EditText) findViewById(R.id.fieldError)).setText(result);
                    }
                    break;
                case MsgId.LOGIN_BY_PASS:
                    User user = (User) msg.obj;
                    if (user != null && ((CheckBox) findViewById(R.id.cbRemember)).isChecked()) {
                        saveUserData(user.getLogin(), user.getPass());
                    }
                    break;
                default:
                    break;
            }
            ;
        }
    }
}
