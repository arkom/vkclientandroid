package ru.myhelpit.vkplayer.utils;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.Calendar;
import java.util.GregorianCalendar;

import ru.myhelpit.vkplayer.Constant;
import ru.myhelpit.vkplayer.R;
import ru.myhelpit.vkplayer.data.User;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by DL on 16.12.2017.
 */

public class PreferenceUtils {
    private static final String TAG = "PreferenceUtils";

    public static final User readUser(Context context) {
        SharedPreferences pref = context.getSharedPreferences(Constant.PREFERENCE_USER_DATA, MODE_PRIVATE);
        if (pref != null) {
            String login = pref.getString(Constant.PREFERENCE_USER_LOGIN, "");
            String pass = pref.getString(Constant.PREFERENCE_USER_PASS, "");
            if (!login.isEmpty() && !pass.isEmpty()) {
                return new User(login, pass);
            }
        }
        return null;
    }

    public static void removeUserData(Application app) {
        SharedPreferences pref = app.getSharedPreferences(Constant.PREFERENCE_USER_DATA, MODE_PRIVATE);
        if (pref != null) {
            SharedPreferences.Editor editor = pref.edit();
            editor.clear();
            editor.commit();
            Notifier.show(app, app.getString(R.string.user_data_removed));
        } else {
            Notifier.show(app, app.getString(R.string.no_user_data));
        }
    }

    public static boolean isCheckUpdateEveryTime(Application app) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(app);
        return pref.getBoolean(Constant.PREF_CHECK_UPDATE, true);
    }

    public static boolean isCheckUpdateEnabled(Application app) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(app);
        return pref.getBoolean(Constant.PREF_CHECK_UPDATE_SWITCH, true);
    }

    public static boolean isDebugMode(Context context) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        return pref.getBoolean(Constant.PREF_DEBUG_MODE, false);
    }

    public static boolean moreThanDayPassedAfterCheckUpdate(Application app) {
        SharedPreferences pref = app.getSharedPreferences(Constant.PREFERENCE_SETTINGS, MODE_PRIVATE);
        long lLatestUpdate = pref.getLong(Constant.PREFERENCE_LATEST_UPDATE, 0);
        return DateUtils.moreThanDayPassed(lLatestUpdate, 1);
    }

    public static boolean moreThanDayAfterShowAd(Context context) {
        SharedPreferences pref = context.getSharedPreferences(Constant.PREFERENCE_SETTINGS, MODE_PRIVATE);
        long lLatestUpdate = pref.getLong(Constant.PREFERENCE_LATEST_AD, 0);
        return DateUtils.moreThanDayPassed(lLatestUpdate, 3);
    }



    public static boolean isNeedCheckUpdate(Application app) {
        return isCheckUpdateEnabled(app) && (isCheckUpdateEveryTime(app) || moreThanDayPassedAfterCheckUpdate(app));
    }

    public static void saveLatestShowAd(Context context, long ms) {
        SharedPreferences pref = context.getSharedPreferences(Constant.PREFERENCE_SETTINGS, MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putLong(Constant.PREFERENCE_LATEST_AD, ms);
        editor.commit();
    }

    public static void saveLatestUpdate(Context context, long ms) {
        SharedPreferences pref = context.getSharedPreferences(Constant.PREFERENCE_SETTINGS, MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putLong(Constant.PREFERENCE_LATEST_UPDATE, ms);
        editor.commit();
    }

    public static int readValidVersion(Context context){
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        return pref.getInt(Constant.PREF_VALID_VERSION_CODE, Constant.DEFAULT_VALID_VERSION);
    }

    public static void saveValidVersion(Context context, int validVersion){
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        if (pref != null) {
            SharedPreferences.Editor editor = pref.edit();
            editor.putInt(Constant.PREF_VALID_VERSION_CODE, validVersion);
            editor.commit();
        } else {
            Log.w(TAG, "shared preference is null");
        }
    }

    public static int getConnectionCheckPeriod(Application app) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(app);
        return pref.getInt(Constant.PREF_CONNECTION_CHECK_PERIOD, 60);
    }

    public static boolean isSaveCookies(Application app) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(app);
        return pref.getBoolean(Constant.PREF_SAVE_COOKIES, true);
    }

    public static String readLastDownloadAddress(Context context) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        return pref.getString(Constant.PREF_LAST_DOWNLOAD_ADDRESS, "");
    }

    public static void saveLastDownloadAddress(Context context, String ipAddress){
        if (ipAddress == null){
            return;
        }
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        if (pref != null) {
            SharedPreferences.Editor editor = pref.edit();
            editor.putString(Constant.PREF_LAST_DOWNLOAD_ADDRESS, ipAddress);
            editor.commit();
        } else {
            Log.w(TAG, "shared preference is null");
        }
    }
}
