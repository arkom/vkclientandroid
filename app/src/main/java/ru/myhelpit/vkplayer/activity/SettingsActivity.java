package ru.myhelpit.vkplayer.activity;


import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.util.Log;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import ru.myhelpit.vkplayer.R;
import ru.myhelpit.vkplayer.VkPlayerApplication;

public class SettingsActivity extends PreferenceActivity {
    private static final String TAG = "SettingsActivity";
    private Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Obtain the shared Tracker instance.
        VkPlayerApplication application = (VkPlayerApplication) getApplication();
        mTracker = application.getDefaultTracker();
        addPreferencesFromResource(R.xml.pref_general);
    }

    @Override
    protected void onResume() {
        Log.i(TAG, "Setting screen name: " + TAG);
        mTracker.setScreenName("Image~" + TAG);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        super.onResume();
    }

}
