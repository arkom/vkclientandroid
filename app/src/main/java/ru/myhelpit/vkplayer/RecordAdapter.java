package ru.myhelpit.vkplayer;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

import ru.myhelpit.vkplayer.data.PlayList;
import ru.myhelpit.vkplayer.data.RecordExt;
import ru.myhelpit.vkplayer.utils.PreferenceUtils;

/**
 * Created by DL on 25.11.2017.
 */

public class RecordAdapter extends BaseAdapter {
    private static final String TAG = "RecordAdapter";
    Context context;
    PlayList playList;
    LayoutInflater inflater;
    private CompoundButton.OnCheckedChangeListener boxChangeListener;

    public RecordAdapter(Context context, PlayList playList) {
        this.context = context;
        this.playList = playList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return playList.size();
    }

    @Override
    public Object getItem(int i) {
        return playList.getByIndex(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        View view = convertView != null ? convertView : inflater.inflate(R.layout.item, viewGroup, false);
        final RecordExt record = playList.getByIndex(position);

        ((TextView) view.findViewById(R.id.name)).setText(record.getName());
        ((TextView) view.findViewById(R.id.executor)).setText(record.getExecutor());

        CheckBox box = view.findViewById(R.id.cbBox);
        box.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RecordExt recordExt = playList.getByIndex(position);
                recordExt.setSelected(!recordExt.isSelected());
                Log.d(TAG, "Item " + recordExt);
                Log.d(TAG, "URL: " + recordExt.getUrl());
                if (PreferenceUtils.isDebugMode(context)) {
                    Toast toast = Toast.makeText(context,
                            "name: " + recordExt.getName()
                                    + "\nURL: " + recordExt.getUrl()
                            , Toast.LENGTH_SHORT);
                    toast.show();
                }

            }
        });
        box.setTag(position);
        box.setChecked(playList.getByIndex(position).isSelected());

        ImageButton btnPlay = view.findViewById(R.id.btnPlay);
        btnPlay.setTag(position);
        btnPlay.setImageResource(record.isPlay() ? R.drawable.ic_action_pause : R.drawable.ic_action_play);


        if (record.getFilePath() != null) {
            view.setBackgroundResource(record.isPlay() ? R.color.colorLightGreen : R.color.white_background);
            btnPlay.setEnabled(true);
        } else if(record.getUrl() != null){
            view.setBackgroundResource(record.isPlay() ? R.color.colorLightGreen : R.color.colorYellow200);
            btnPlay.setEnabled(true);
        } else{
            view.setBackgroundResource(R.color.colorGrey200);
            btnPlay.setEnabled(false);
        }

        ImageView album = view.findViewById(R.id.imgAlbum);
        Bitmap myBitmap = null;
        if (record.getImage40x40Path() != null) {
            File imgFile = new File(record.getImage40x40Path());
            if (imgFile.exists()) {
                myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            }
        }
        if (myBitmap != null) {
            album.setImageBitmap(myBitmap);
        } else {
            album.setImageResource(R.drawable.ic_stat_);
            album.setBackgroundColor(Color.GRAY);
        }

        return view;
    }
}
