package ru.myhelpit.vkplayer;

import android.app.Application;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;

import java.io.DataOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;

import ru.myhelpit.vkclient.controller.VkClient;
import ru.myhelpit.vkclient.controller.VkHttpConnectionImpl;
import ru.myhelpit.vkplayer.data.VkError;
import ru.myhelpit.vkplayer.utils.AppUtils;
import ru.myhelpit.vkplayer.utils.PreferenceUtils;
import ru.myhelpit.vkplayer.utils.VkCookieSaver;

import static java.lang.String.format;

/**
 * Created by DL on 06.01.2018.
 */

/**
 * This is a subclass of {@link Application} used to provide shared objects for this app, such as
 * the {@link Tracker}.
 */
public class VkPlayerApplication extends Application {
    private Tracker mTracker;
    private VkClient vkClient;
    private static final String TAG = "VkPlayerApplication";


    @Override
    public void onCreate() {
        final Thread.UncaughtExceptionHandler oldHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable throwable) {
                String s = AppUtils.getStackTraceAsString(throwable);
                handleError(s, oldHandler, thread, throwable);
            }
        });
        super.onCreate();
    }

    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     *
     * @return tracker
     */
    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            mTracker = analytics.newTracker(R.xml.global_tracker);
        }
        return mTracker;
    }

    synchronized public VkClient getVkClient() {
        if (vkClient == null) {
            VkHttpConnectionImpl client = new VkHttpConnectionImpl(PreferenceUtils.isSaveCookies(this));
            client.setHttpCookieSaver(new VkCookieSaver(getApplicationContext()));
            vkClient = new VkClient(client);
        }
        return vkClient;
    }

    private void handleError(final String report,
                             final Thread.UncaughtExceptionHandler oldHandler,
                             Thread thread, Throwable throwable) {
        Thread t = new Thread(() -> {
            long start = System.currentTimeMillis();
            AppUtils.sendReport(report, throwable, getApplicationContext(), "!!!APP CRASH!!!");
            Log.d(TAG, format("sending logs took %d ms", System.currentTimeMillis() - start));
        });
        t.start();

        try {
            t.join(5 * 1000);
        } catch (InterruptedException e) {
            Log.e(TAG, "error: ", e);
        }
        oldHandler.uncaughtException(thread, throwable);
    }
}
